import numpy as np
import pandas as pd

import os
import csv

from tensorflow.keras.preprocessing.text import Tokenizer

from crayons.regex_crayons import *
from secxxxy_edgar_vectors.edgar_regex_patterns import *

def extract_and_replace_numbers(text:str, number_regex:list, regex_extraction_indices:list=[]):
    extracted = []
    texts = [text]
    found = []
    replaced = []
    for i, (compiled, replacement) in enumerate(number_regex):
        if i not in regex_extraction_indices:
            for j in range(len(texts)):
                texts[j] = compiled.sub(replacement, texts[j])
            continue
        found_temp = []
        replaced_temp = []
        substrings = []
        for j in range(len(texts)):

            indices = [0]
            for match in compiled.finditer(texts[j]):
                indices.extend(match.span())
                found_temp.append(match.group())
                replaced_temp.append(replacement(match))
            # Generator will give bug if empty so checking to see if the index list has been changed.
            if len(found_temp) > 0:
                indices.append(None)
                substrings.extend([texts[j][indices[k]:indices[k+1]] for k in range(len(indices)-1) if not k%2])
            else:
                substrings.append(texts[j])

            if len(found) > 0 and j != len(texts) - 1:
                found_temp.append(found[j])
                replaced_temp.append(replaced[j])
        found = found_temp
        replaced = replaced_temp
        texts = substrings
    return found, replaced, texts


def generate_edgar_tokenizer(vocabulary_path:str, oov_token:str='NULL'):
    vocabulary = pd.read_csv(vocabulary_path, sep=' ', quoting=csv.QUOTE_NONE, header=None, usecols=[0], converters={0:str}).squeeze()
    tokenizer = Tokenizer(filters='', lower=False, split=None, oov_token=oov_token)
    tokenizer.fit_on_texts(vocabulary)
    return tokenizer

def set_window_boundaries(window_size:int):
    '''The token of interest will be placed into the center of the window for the neural network. Can only be symmetric for odd numbers.
    Since words before carry slightly more importance in classication they will be favored when symmetrical splits cannot be made.'''
    if window_size % 2:
        return window_size//2, window_size//2
    else:
        return window_size//2, window_size//2 - 1

def convert_token_array_to_windowed_lines(token_array, indices, left, right):
    left_min, right_max = indices[0] - left, indices[-1] + right
    if left_min < 0 or right_max >= len(token_array):
        offset = abs(left_min)
        token_array = np.pad(token_array, [max([0, 0-left_min]), max([0, right_max + 1 - len(token_array)])], constant_values='NULL')
    else:
        offset = 0
    selection = np.concatenate([np.expand_dims(indices, axis=1) + (v + offset) for v in range(-left,right+1)], axis=1)
    context_array = token_array[selection]
    return context_array

def step_and_aggregate_condition_over_2nd_dim(match_array, start:int, end:int, reverse:bool=False):
    step = -1 if reverse else 1
    for i in range(start + step, end, step):
        match_array[:,i] = match_array[:,i-step] | match_array[:,i]
    return match_array

def drop_token_context_array_values_outside_of_seperator(context_array, seperator:str, left:int, right:int):
    matches = context_array == seperator
    columns = context_array.shape[1]
    matches = step_and_aggregate_condition_over_2nd_dim(matches, start=left-1, end=-1, reverse=True)
    matches = step_and_aggregate_condition_over_2nd_dim(matches, start=columns-right, end=columns)
    return np.where(matches, 'NULL', context_array)

def generate_edgar_windowed_array_from_text(tokens:list, extracted:list, replaced:list, window_size:int, cutoff_token:str='',):
    
    left, right = set_window_boundaries(window_size)
    token_array = np.array(tokens)
    change_indices = np.in1d(token_array, pd.Series(replaced).unique()).nonzero()[0]
    context_array = convert_token_array_to_windowed_lines(token_array, change_indices, left, right)
    if cutoff_token:
        context_array = drop_token_context_array_values_outside_of_seperator(context_array, cutoff_token, left, right)
        
    return context_array


def edgar_text_preprocessing_common_mid(text:str, extended=False):
    if extended:
        regex = SPACES_AND_BREAK_REGEX_LIST_EXTENDED
    else:
        regex = SPACES_AND_BREAK_REGEX_LIST
    for compiled, replacement in regex:
        text = compiled.sub(replacement, text)
    text = text.lower().strip()
    text = URL_REGEX.sub('URL', text)
    return regex_keyword_dict_replacement(text, ABBREVIATION_REGEX, ABBREVIATIONS, ignore_case=True)

def edgar_text_preprocessing_common_end(text, extended=False):
    if extended:
        regex = ML_FORMAT_REGEX_EXTENDED
    else:
        regex = ML_FORMAT_REGEX
    text = REPEAT_REGEX.sub(r'\1\1', text)
    text = SEPERATE_SYMBOL_REGEX.sub('\r' + r'\1' + '\r', text)
    text = looped_string_replacement(text, regex)
    return SPACE_CLEAN_REGEX.sub(' ', text).strip()


def regex_text_preprocessing_with_number_splits(text:str, regex_extraction_indices:list, window_size:int, cutoff_token:str,
                                                match_keywords:list=[]):
    extracted, replaced, texts = extract_and_replace_numbers(text, NUMBER_REGEX_FUNC, regex_extraction_indices)
    
    tokens = []
    for i, text in enumerate(texts):
        text = text.strip('\r')
        if (not text or text == ' ' or text == '\n') and not i == len(texts) -1:
            tokens.append(replaced[i])
            continue
        text = edgar_text_preprocessing_common_end(text, extended=True)
        text = text.split(' ')
        if i == len(texts) -1:
            tokens.extend(text)
        else:
            tokens.extend(text + [replaced[i]])
            
    tokens = generate_edgar_windowed_array_from_text(tokens, extracted, replaced, window_size, cutoff_token)
    return tokens, extracted, replaced
    if len(match_keywords) > 0:
        has_keywords = np.any(np.isin(tokens, match_keywords), axis=1)
        tokens = tokens[has_keywords]
        extracted = [extracted[i] for i in range(has_keywords.size) if has_keywords[i]]
        replaced = [replaced[i] for i in range(has_keywords.size) if has_keywords[i]]
    return tokens, extracted, replaced


def create_labeling_csv_append_in_steps(token_array, extracted:list, replaced:list, path:str, filename_col_value:str,
                                        token_substitutions:dict={}, default_label:int=None, csv_sep:str=','):
    '''Creates a csv for convienient labeling and and appends new values to it. Can be placed in loops to write directly avoiding memory issues.'''
    if not os.path.exists(path):
        df = pd.DataFrame(columns=['filename', 'text_left', 'original', 'substituted', 'text_right', 'label'])
        df.to_csv(path, index=False, sep=csv_sep)
    df = pd.DataFrame(extracted, columns=['original',])
    df.insert(0, 'filename', filename_col_value)
    string_df = pd.DataFrame(token_array)
    for key, value in token_substitutions.items():
        string_df.replace(key, value, inplace=True)
    center_index = string_df.shape[1]//2
    string_df_left = string_df[0].str.cat(string_df.iloc[:,1:center_index], sep=' ').str.strip()
    string_df_left.name = 'text_left'
    string_df_right = string_df[center_index+1].str.cat(string_df.iloc[:,center_index+2:], sep=' ').str.strip()
    string_df_right.name = 'text_right'
    string_df = string_df[center_index]
    string_df.name = 'substituted'
    df.index = string_df.index
    df = pd.concat([df, string_df, string_df_right], axis=1)
    df['label'] = default_label
    df.insert(1, 'text_left', string_df_left)
    df.to_csv(path, mode='a', index=False, header=False, sep=csv_sep)