import re

from crayons.regex_crayons import regex_keyword_combiner

def add_ampersand_entity_fix_key_layers(regex_replacement_dict:dict, layers:int=2):
    for _ in range(layers):
        regex_replacement_dict.update({'&amp;' + key[1:]:value for key, value in regex_replacement_dict.items() if key[0]=='&'})

def number_regex_replace_commas_and_periods_small(x):
    if x[1] == '0' or not x[1]:
        return 'F0D'
    elif x[1][0:1] == '0' or len(x[1]) > 3:
        return 'XN'
    elif len(x[2]) == 3:
        return 'I' + str(len(x[1])+3) + 'D'
    else:
        return 'F' + str(len(x[1])) + 'D'
    
def number_regex_replace_commas_and_periods_large(x):
    # First grouping should range from 1-999:

    if x[1][0:1] == '0' or len(x[1]) > 3:
        return 'XN'
    sep1, sep2, m = x[2], x[4], x[3]
    if sep1 != sep2:
        kind = 'F'
        s = x[0].rsplit(sep2, maxsplit=1)[0]
        s = s.replace(sep2, sep1)
        m = m.replace(sep2, sep1)
    else:
        kind = 'I'
        s = x[0]
        # Process is accepting of wrongly placed seperators in floats, in ints it will not be tolerated
        if len(s.split(',.'.replace(sep1, ''))) != 1:
            return 'XN'
    # Between seperators should have groups of 3 numbers
    if not all([len(y) == 3 for y in m.split(sep1)]):
        return 'XN'
    
    digits = len(s.replace(sep1, ''))
    if digits < 7:
        digits = str(digits) + 'D'
    elif digits < 10:
        digits = 'M'
    else:
        digits = 'BPLUS'
    return kind + digits

def number_regex_replace_numbers_only(x):
    if len(x[0]) > 8:
        return 'N9PLUS'
    elif len(x[0]) > 3:
        return 'N' + str(len(x[0])) + 'D'
    elif x[0][0:1] != '0':
        return 'I' + str(len(x[0])) + 'D'
    else:
        return 'N23D'

ATTACHMENT_REMOVAL_REGEX1 = re.compile(r'\n *begin\s+\d{1,8}\s+[\w:/~\.\[\]\-\\]{1,100}\.(?:gif|jpg|pdf|xls|zip) *\n(?:.*\n)+? *(?=end\b)end', flags=re.I)
ATTACHMENT_REMOVAL_REGEX2 = re.compile(r'\n *<FILENAME>\w{1,30}\.(?:pdf|xlsx?|zip) *\n(?:.*\n)+? *(?=</DOCUMENT>)</DOCUMENT>', flags=re.I)

SINGLE_QUOTE_CHARS = ['‘', '’'] + ['&' + ent + ';' for ent in ['lsquo', 'rsquo', 'apos', '#145', '#146', '#8216', '#8217', '#x2018', '#x2019']]
DOUBLE_QUOTE_CHARS = ['“', '”'] + ['&' + ent + ';' for ent in ['ldquo', 'rdquo', 'quot', 'quote', '#147', '#148', '#8220', '#8221', '#x201c', '#x201d']]
HTML_ENTITY_DICT = ['acute', 'numsp', '&shy', '8218', '8219', '#0', '#129', '#141', '#143', '#144', '#157', '#55349', '#56129', '#56144',
                    '#56157', '#56256', '#56320', '#56374', '#56375', '#56380', '#56389', '#56410', '#56412', '#56417', '#56421', '#61472',
                    '#65533', '#-3884', '#-4057', '#-4064;', '#x9d',]
HTML_ENTITY_DICT = {key:'' for key in ['&' + ent + ';' for ent in HTML_ENTITY_DICT]}
HTML_ENTITY_DICT.update({**{quote:"'" for quote in SINGLE_QUOTE_CHARS}, **{quote:'"' for quote in DOUBLE_QUOTE_CHARS}})
HTML_ENTITY_DICT.update({'&#38;':'&', '&#167;':'§', '&amp;':'&', '&nbps;':' ',  '&nbsp;':' ', '&inbsp;':' ', '&percnt;':'%', '&eacute;':'é',
                         '&half;':'1/2', '&sect;':'§', '&ouml;':'ö', '&imacr;':'ī', '&commat;':'@', '&fflig;':'ff', '&filig;':'fi',
                         '&ndash;':'–', '&mdash;':'—', '&squarf;':'▪',  '&middot;':'·', '&cir;':'○',})
add_ampersand_entity_fix_key_layers(HTML_ENTITY_DICT)
HTML_ENTITY_REGEX = regex_keyword_combiner(HTML_ENTITY_DICT, compile_kwargs={'flags':re.I})

SPACE_CHAR_DICT = {'<br>':'\n', '<BR>':'\n', '\r':'\n', '\t':'  ', '\f':' ', '\u200a':' ', '\u2002':' ', '\u2003':' ', '\u2005':' ',
                   '\u2006':' ', '\u2007':' ', '\u2008':' ', '\u2009':' ', '\u2029':' ', '\u3000':' ',}
SPACE_CHAR_REGEX = regex_keyword_combiner(SPACE_CHAR_DICT, compile_kwargs={'flags':re.I})


SPACES_AND_BREAK_REGEX_LIST = [[r'[^\S\n]', ' '], [r' {2,}', '  '], [r' *\n *', '\n'], [r'\n{2,}', '\n\n']]
SPACES_AND_BREAK_REGEX_LIST = [[re.compile(pattern), replacement] for pattern, replacement in SPACES_AND_BREAK_REGEX_LIST]
SPACES_AND_BREAK_REGEX_LIST_EXTENDED = SPACES_AND_BREAK_REGEX_LIST.copy()
SPACES_AND_BREAK_REGEX_LIST_EXTENDED[-1] = [re.compile(r'\n{3,}'), '\n\n\n']

UNICODE_REMOVAL_REGEX = ['\x00', '\x01', '\x02', '\x03', '\x04', '\x06', '\x08', '\x0e', '\x0f', '\x10', '\x11', '\x15', '\x18', '\x1b',
                         '\x1c', '\x1f', '\x7f', '\xa0', '\xad', '\U0003dd45', '\u0df6', '\u0ff4', '\ufeff', '\u200b', '\u200c', '\u200d',
                         '\u200e', '\u200f', '\u202a', '\u202c', '\u202d', '\u202e', '\u202f', '\u2028', '\u206a', '\u206b', '\u206c',
                         '\u206d', '\u206f', '\udc89', '\ue821', '\ue83a', '\uf004', '\uf008', '\uf00a', '\uf00d', '\uf00e', '\uf011',
                         '\uf01d', '\uf020', '\uf023', '\uf024', '\uf027', '\uf028', '\uf029', '\uf02a', '\uf02c', '\uf02d', '\uf02e',
                         '\uf02f', '\uf030', '\uf031', '\uf032', '\uf033', '\uf034', '\uf035', '\uf036', '\uf037', '\uf038', '\uf039',
                         '\uf03a', '\uf03d', '\uf043', '\uf048', '\uf04a', '\uf051', '\uf053', '\uf054', '\uf05b', '\uf05d', '\uf05f',
                         '\uf061', '\uf06c', '\uf06f', '\uf072', '\uf074', '\uf075', '\uf076', '\uf078', '\uf07e', '\uf097', '\uf098',
                         '\uf09f', '\uf0a0', '\uf0a7', '\uf0a8', '\uf0b0', '\uf0b3', '\uf0b7', '\uf0b8', '\uf0be', '\uf0d2', '\uf0d4',
                         '\uf0d6', '\uf0d8', '\uf0e2', '\u0eff', '\uf0fd', '\uf0fe', '\uf818', '\uf850', '\ufdd3',
                         '\u0fe0', '\ufeff', '\u0ff2', '\u0ff3', '\u0ff6']
UNICODE_REMOVAL_REGEX = regex_keyword_combiner(UNICODE_REMOVAL_REGEX)

URL_REGEX = re.compile(r'\b(?:(?:https?|ftp):\/\/|www\.)(?:\w{2,}\.)+\w{2,}(?:\/\w+)*(?:\.html?|\/)?(?!\w)', flags=re.I)

ABBREVIATIONS = {'I.R.S.':'irs', 'U.K.':'uk', 'U.S.':'usa', 'U.S.A.':'usa', 'D.C.':'dc', 'N.W.':'nw', 'I.E.':'ie', 'N.V.':'nv',
                 'A.M.':'aDOTm', 'P.M.':'pm', 'C.F.R.':'cfr', 'U.S.C.':'usc', 'W.R.':'wr', 'B.A.':'ba', 'P.O.':'po', 'L.L.C.':'llc',
                 'L.E.':'le', 'R.F.C.':'rfc', 'R.G.':'rg', 'A.S.R.L.':'asrl',  'G.T.A.':'gta', 'S.A.R.L.':'sarl', 'S.A.':'sa',
                 'S.A.R.':'sar', 'N.C.':'nc', 'E.G.':'eg', 'B.N.Y.':'bny', 'A.I.':'ai', 'P.J.':'pj', 'L.W.':'lw', 'N.E.':'ne',
                 'K.K.':'kk', 'D.E.':'de', 'D.A.':'da', 'E.B.':'eb', 'B.J.':'bj', 'W.D.':'wd', 'A.J.':'aj', 'E.M.':'em', 'M.D.':'md',
                 'L.P.':'lp', 'C.T.S.':'cts', 'N.T.':'nt', 'L.T.':'lt', 'P.I.A.':'pia', 'B.T.':'bt', 'R.H.S.C.':'rhsc', 'V.P.':'vp',
                 'E.V.P.':'evp', 'J.P.':'jp', 'P.U.':'pu', 'F.C.C.':'fcc', 'P.U.C.':'puc', 'I.D.':'id', 'S.J.':'sj', 'R.T.':'rt',
                 'J.D.':'jd', 'G.H.':'gh', 'B.F.':'bf', 'O.E.':'oe', 'T.B.':'tb', 'F.D.I.C.':'fdic', 'C.V.':'cv', 'V.F.':'vf',
                 'N.J.S.A.':'njsa', 'C.W.':'cw', 'F.A.C.':'fac', 'I.R.A.':'ira', 'F.A.':'fa', 'B.S.C.':'bsc', 'P.L.':'pl', 'R.P.':'rp',
                 'S.W.':'sw', 'N.Y.':'ny', 'R.J.':'rj', 'S.E.':'se', 'W.F.S.':'wfs', 'A.D.':'ad', 'S.S.':'ss', 'S.P.A.':'spa',
                 'S.F.':'sf', 'B.V.':'bv', 'M.B.H.':'mbh', 'B.C.':'bc', 'W.H.':'wh', 'F.A.I.A.':'faia','G.B.':'gb', 'A.R.':'ar',
                 'S.R.L.':'srl', 'O.C.':'oc', 'L.M.':'lm', 'G.G.':'gg', 'J.M.':'jm', 'R.E.':'re', 'F.E.':'fe', 'C.A.':'ca', 'H.P.':'hp',
                 'R.N.':'rn', 'G.M.':'gm', 'F.S.B.':'fsb', 'P.A.':'pa', 'D.F.':'df', 'L.A.':'la', 'F.O.B.':'fob', 'A.E.B.T.':'aebt',
                 'F.I.C.A.':'fica', 'N.M.':'nm', 'J.A.M.S.':'jams', 'N.H.':'nh', 'L.L.P.':'llp', 'V.N.J.':'vjn', 'R.D.':'rd',
                 'H.R.':'hr', 'A.C.':'ac', 'J.B.':'jb', 'M.R.':'mr', 'I.R.C.':'irc', 'C.O.D.':'cod', 'C.E.':'ce', 'I.R.':'ir',
                 'W.W.':'ww', 'J.D.H.':'jdh', 'A.G.':'ag', 'C.E.O.':'ceo', 'S.A.R.L.':'sarl', 'S.R.O.':'sro', 'B.S.M.':'bsm',
                 'D.C.N.J.':'dcnj', 'S.S.N.':'ssn', 'T.H.':'th', 'C.R.I.':'cri', 'J.L.':'jl', 'R.W.':'rw', 'B.V.B.A.':'bvba', 'S.I.':'si',
                 'I.D.C.S.':'idcs', 'H.L.':'hl', 'T.L.':'il', 'O.S.':'os', 'S.M.':'sm', 'S.S.B.':'ssb', 'F.T.Q.E.':'ftqe', 'E.I.':'ei',
                 'N.J.':'nj', 'P.L.C.':'plc', 'S.A.P.':'sap', 'D.L.':'dl', 'I.C.':'ic', 'I.B.':'ib', 'I.A.':'ia', 'L.C.':'lc',
                 'C.P.A.':'cpa', 'C.O.O.':'coo', 'F.O.':'fo', 'W.P.':'wp', 'N.A.':'na', 'B.S.':'bs', 'P.C.':'pc', 'M.B.A.':'mba',
                 'M.S.':'ms', 'A.B.':'ab', 'M.A.':'ma', 'B.B.A.':'bba', 'L.L.':'ll', 'R.L.':'rl', 'S.C.':'sc', 'J.C.':'jc', 'S.L.':'sl',
                 'G.P.':'gp', 'A.S.':'aDOTs', 'R.R.':'rr', 'S.E.C.':'sec', 'M.B.':'mDOTb', 'C.F.O.':'cfo', 'C.R.':'cr', 'J.H.':'jh',
                 'P.R.':'pr', 'S.A.S.':'sas', 'P.E.':'pe', 'D.B.A.':'dba', 'S.R.':'sr', 'S.P.':'sp', 'J.W.':'jw', 'J.R.':'jr',
                 'J.A.':'ja', 'P.S.':'ps', 'C.F.':'cf', 'H.C.':'hc', 'C.P.':'cp', 'N.D.':'nd', 'H.K.':'hk', 'R.S.':'rs', 'S.D.':'sd',
                 'R.H.':'rh', 'H.J.':'hj', 'E.F.':'ef', 'E.D.':'eDOTd', 'B.S.E.E.':'bsee', 'S.D.N.Y.':'sdny', 'P.T.':'pt', 'L.S.':'ls',
                 'C.H.':'ch', 'T.J.':'tj', 'A.T.':'aDOTt', 'S.B.':'sb', 'R.C.':'rc', 'C.B.':'cb', 'W.L.':'wl', 'J.E.':'je', 'G.E.':'ge',
                 'F.W.':'fw', 'J.J.':'jj', 'G.D.':'gd', 'M.J.':'mj', 'A.P.':'ap', 'S.A.C.':'sac', 'B.M.':'bm', 'R.A.':'ra',
                 'C.D.':'cDOTd', 'H.B.':'hb', 'C.S.':'cs', 'M.I.T.':'mit', 'W.A.':'wa', 'W.T.':'wt', 'R.M.':'rm', 'E.J.':'ej',
                 'C.C.':'cc', 'J.F.':'jf', 'L.L.M.':'llm', 'E.W.':'ew', 'M.S.E.E.':'msee', 'L.G.':'lg', 'C.J.':'cj', 'L.L.B.':'llb',
                 'E.U.':'eu', 'L.F.':'lf', 'J.V.':'jv', 'D.D.S.':'dds', 'W.C.':'wc', 'B.B.':'bb', 'H.W.':'hw', 'U.S.C.A.':'usca',
                 'C.M.':'cm', 'G.M.B.H.':'gmbh', 'W.J.':'wj', 'S.C.A.':'sca', 'F.K.A.':'fka', 'E.A.':'ea', 'P.R.C.':'prc',
                 'R.O.C.':'roc', 'C.I.':'ci', 'M.H.':'mh', 'W.E.':'wDOTe', 'H.H.':'hh', 'B.E.':'bDOTe', 'H.F.':'hf', 'P.L.L.C.':'pllc',
                 'D.H.':'dh', 'T.V.':'tv', 'J.S.':'js', 'R.B.':'rb'}
ABBREVIATIONS = {key.lower():value for key, value in ABBREVIATIONS.items()}
ABBREVIATION_REGEX = regex_keyword_combiner(ABBREVIATIONS, left_append=r'(?<!\w|\.)', right_append=r'(?!\w)', compile_kwargs={'flags':re.I})

MONTH_PATTERN = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec']
MONTH_PATTERN = regex_keyword_combiner(MONTH_PATTERN, left_append='(?:', right_append=')', compiled=False)
MONTH_PATTERN = r'(?:' + MONTH_PATTERN + r'([\-\/])[0-3]\d\1|(?:[0-3]?\d\/){2})(?:[12]\d)?\d{2}'
MONTH_PATTERN_LONG = ['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december']
MONTH_PATTERN_LONG = regex_keyword_combiner(MONTH_PATTERN_LONG, left_append='(?:', right_append=')', compiled=False)
MONTH_PATTERN_LONG += r'\s*[0-3]?\d(?:,?\s+|,\s*)[12]\d{3}'
MONTH_PATTERN = r'\b(?:' + MONTH_PATTERN + '|' + MONTH_PATTERN_LONG + r')\b'

NUMBER_REGEX = [[MONTH_PATTERN, 'DATE'], [r'\b\d+(?:-\d+)*-?(?:(?<!-)-\d{2,}|\d{2,}-(?!-))+-?(?:\d+-)*\d+\b', 'NHYPHEN'], ['\bx{3,}\d{2,}', 'HN'],
                [r'\b([a-z]{1,2})\1*(?:\d{2,}|(?:\d+[a-z])+\d?)\b', lambda x: 'N' + x[1].upper()], [r'\b(?:\d+[a-z]){2,}\d*\b', 'MN'],
                [r'([\.,]?(?:\d+[\.,]?){2,})', '\r' + r'\1' + '\r'],
                [r'\b(\d+)(\.|,)((?:\d+[\.,])*\d+)(?:(\.|,)\d+)\b(?![\.,]\d)', number_regex_replace_commas_and_periods_large], 
                [r'(?:\b(\d+))?[\.,](\d+)\b', number_regex_replace_commas_and_periods_small],
                [r'\b\d{2,}\b', number_regex_replace_numbers_only]]
NUMBER_REGEX = [[re.compile(pattern), replacement] for pattern, replacement in NUMBER_REGEX]

NUMBER_REGEX_FUNC = [[compiled, replacement] if not isinstance(replacement, str) else ([compiled, lambda x: '\r' + x[1] + '\r'] if i == 5 else [compiled, lambda x, y=replacement: y]) for i, (compiled, replacement) in enumerate(NUMBER_REGEX)]
    
REPEAT_REGEX = re.compile(r'([=\-\*#\+—–…\?,~\\†\^@\/>\$&\!\|%■‡_\._☐▬▪═−€•±«¨§£x](?:\s?\s)?)\1{2,}')
SEPERATE_SYMBOL_REGEX = re.compile(r'(([^a-zA-Z\u00C0-\u00FF\d\s])\2?)')

ML_FORMAT_REGEX = {'  ':' DS ', '\n\n':' DL ', '\r':' ',}
ML_FORMAT_REGEX_EXTENDED = {'  ':' DS ', '\n\n\n':' DL ', '\r':' ',}
SPACE_CLEAN_REGEX = re.compile(r'\s+')

CLEAN_TAGS_REGEX = [[r'<\/?(?:font|span)(?:\s+(?:[^>])*?style=[^>]*?)?>', ''], [r'<\!\[CDATA\[[\s\S]*?\]\]>', ''],
                    [r'<(\w+)\s+(?:[^>])*?(?:style|align|color|padding)=[^>]*?>', r'<\1>'], [r'<(?:\/p><p|br)>|&nbsp;', ' '],
                    ['<\/?[bip]>', '']]
CLEAN_TAGS_REGEX = [[re.compile(pattern, flags=re.I), replacement] for pattern, replacement in CLEAN_TAGS_REGEX]
    
### OLD

def regex_int_replacement(x):
    digits = len(x[0].replace(x[1], ''))
    if digits < 7:
        digits = str(digits) + 'D'
    elif digits < 10:
        digits = 'M'
    else:
        digits = 'BPLUS'
    return 'I' + digits

def regex_float_replacement(x):
    digits = len(x[0].rsplit(x[2], maxsplit=1)[0].replace(x[1], '').replace(x[2], ''))
    if digits < 7:
        digits = str(digits) + 'D'
    elif digits < 10:
        digits = 'M'
    else:
        digits = 'BPLUS'
    return 'F' + digits


obsolete_number_regex = [[r'\b[1-9]\d{0,2}(\.|,)(?:\d{3}[\.,])*?\d{3}(?!\1)([\.,])\d{1,}\b(?![\.,]\d)', regex_float_replacement], 
                        [r'\b[1-9]\d{0,2}(\.|,)(?:\d{3}\1)*\d{3}\b', regex_int_replacement],]
#ml_format = {'  ':' DS ', '\n\n':' DL ', '\n':' L ', '\r':' ',}