# Edgar Your Vectors Are SECXXXy

**EXPERIMENTAL PROJECT DEPRECATED**

Word embeddings trained on 6 Billion+ Tokens from a partially class balanced random sample of 240,000+ Edgar 10K, 10Q, and comparable variations of those forms from 1994-2022 using Stanford's GloVe word co-occurrence model.

Vocabulary size: 487,646
Ndims: 100
Window Size: 16
Xmax: 30:
Training Epochs: 1000

Repository to contain machine learning models using those vectors
